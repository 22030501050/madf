import 'package:flutter/material.dart';
class BookingSignIn extends StatefulWidget {
  const BookingSignIn({super.key});

  @override
  State<BookingSignIn> createState() => _BookingSignInState();
}

class _BookingSignInState extends State<BookingSignIn> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset(fit: BoxFit.cover, "assets/Images/IMG_20221101_220806.jpg"),
          Container(
            color: Colors.black54,
          ),
          Row(
            children: [
              Expanded(
                child: Container(),

              ),
            ],
          ),
          Expanded(
              child: Column(
            children: [
              SizedBox(
                height: 15,
              ),
              Container(
                 margin: EdgeInsets.only(left:17,top: 60),
                  child: Text("Booking  ",
                     style: TextStyle(
                       color: Colors.grey,
                       fontSize: 55,
                       fontWeight: FontWeight.bold,
                     )),
               ),
              Container(
                 margin: EdgeInsets.only(top: 10),
                 child: Text("Book Your Destination Is  ",
                     style: TextStyle(
                       color: Colors.white,
                       fontSize: 15,
                     )),
               ),
               Container(
                 margin: EdgeInsets.only(top: 10),
                 child: Text("Here...!! ",
                    style: TextStyle(
                       color: Colors.white,
                       fontSize: 15,
                     )),
               ),
              SizedBox(
                height: 560,
              ),
              TextButton(
                onPressed: () {
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(
                  //     builder: (context) => VitalProductList(),
                  //   ),
                  // );
                },
                child: Container(
                  width: 350,
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.redAccent,
                  ),
                  child: Center(
                    child: Text(
                      'Sign in With Phone Number  ',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              TextButton(
                onPressed: () {
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(
                  //     builder: (context) => VitalProductList(),
                  //   ),
                  // );
                },
                child: Container(
                  width: 350,
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(child: Icon(Icons.apple_sharp,color: Colors.black,)),
                      Container(
                        padding: EdgeInsets.all(5),
                        child: Text('Sign In  With Apple   ',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                            )),
                      ),
                    ],
                  ),
                ),
              ),

              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(margin: EdgeInsets.only(bottom: 18),
                    child: TextButton(
                        onPressed: () {},
                        child: Text(
                          'Dont Have A Acount Then ? Sign Up ',
                          style: TextStyle(color: Colors.white),
                        ),),
                  ),

                ],
              ),

            ],
          ))
        ],
      ),
    );
  }
}
