import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class BookingCart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: ListView(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(50),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.red.withOpacity(0.3),
                          spreadRadius: 2,
                          offset: Offset(0, 3),
                          blurRadius: 5,
                        )
                      ],
                    ),
                    // child: TextButton(
                    //   style: ButtonStyle(
                    //       backgroundColor: MaterialStateProperty.all(Colors.red)),
                    //   // onPressed: ()
                    //   // {
                    //   //   Navigator.pop(
                    //   //     context,
                    //   //     MaterialPageRoute(
                    //   //       builder: (context) => VitalProductList(),
                    //   //     ),
                    //   //   );
                    //   // },
                    //  child: Icon(Icons.arrow_back,color: Colors.white,),
                    //
                    // ),
                  ),

                  InkWell(
                    onTap: () {},
                    child: Container(
                        margin: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 2,
                                offset: Offset(0, 3),
                                blurRadius: 10,
                              )
                            ]),
                        child: Icon(Icons.notification_add_rounded)),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 20, left: 10, bottom: 10),
              child: Text(
                "Select Destination ",
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.symmetric(vertical: 10),
              width: 380,
              height: 120,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.redAccent.withOpacity(0.6),),
                borderRadius: BorderRadius.circular(0),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.3),
                    spreadRadius: 2,
                    offset: Offset(4,11),
                    blurRadius: 10,
                  )
                ],
              ),
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 5),
                    child: Image.asset("assets/Images/1.jpg"),
                  ),
                  Container(
                    width: 210,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.only(top: 8, left: 10),
                          child: Text(
                            "Udaipur",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 10),
                          child: Text(
                            "Explore Udaipur 4D/5N",
                            style: TextStyle(
                              fontSize: 12,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 8),
                          padding: EdgeInsets.only(left: 10),
                          child: Text(
                            "RS 18000",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,


                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 0),
                        padding: EdgeInsets.all(4),

                        decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(
                            10,
                          ),
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [

                            Text(
                              "Click here",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20),
                            ),

                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.symmetric(vertical: 10),
              width: 380,
              height: 120,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.redAccent.withOpacity(0.6),),
                borderRadius: BorderRadius.circular(0),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.3),
                    spreadRadius: 2,
                    offset: Offset(4,11),
                    blurRadius: 10,
                  )
                ],
              ),
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 5),
                    child: Image.asset("assets/Images/2.jpg"),
                  ),
                  Container(
                    width: 210,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.only(top: 8, left: 10),
                          child: Text(
                            "Jaipur",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 10),
                          child: Text(
                            "Explore Jaipur 7D/6N",
                            style: TextStyle(
                              fontSize: 12,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 8),
                          padding: EdgeInsets.only(left: 10),
                          child: Text(
                            "RS 15000",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,


                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 0),
                        padding: EdgeInsets.all(4),

                        decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(
                            10,
                          ),
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [

                            Text(
                              "Click here",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20),
                            ),

                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.symmetric(vertical: 10),
              width: 380,
              height: 120,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.redAccent.withOpacity(0.6),),
                borderRadius: BorderRadius.circular(0),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.3),
                    spreadRadius: 2,
                    offset: Offset(4,11),
                    blurRadius: 10,
                  )
                ],
              ),
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 5),
                    child: Image.asset("assets/Images/3.jpg"),
                  ),
                  Container(
                    width: 210,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.only(top: 8, left: 10),
                          child: Text(
                            "Jodhpur",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 10),
                          child: Text(
                            "Explore Jodhpur 5D/4N",
                            style: TextStyle(
                              fontSize: 12,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 8),
                          padding: EdgeInsets.only(left: 10),
                          child: Text(
                            "RS 12000",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,


                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 0),
                        padding: EdgeInsets.all(4),

                        decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(
                            10,
                          ),
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [

                            Text(
                              "Click here",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20),
                            ),

                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.symmetric(vertical: 10),
              width: 380,
              height: 120,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.redAccent.withOpacity(0.6),),
                borderRadius: BorderRadius.circular(0),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.3),
                    spreadRadius: 2,
                    offset: Offset(4,11),
                    blurRadius: 10,
                  )
                ],
              ),
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 5),
                    child: Image.asset("assets/Images/4.jpg"),
                  ),
                  Container(
                    width: 210,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.only(top: 8, left: 10),
                          child: Text(
                            "Bikaner",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 10),
                          child: Text(
                            "Explore Bikaner 5D/4N",
                            style: TextStyle(
                              fontSize: 12,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 8),
                          padding: EdgeInsets.only(left: 10),
                          child: Text(
                            "RS 12000",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,


                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 0),
                        padding: EdgeInsets.all(4),

                        decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(
                            10,
                          ),
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [

                            Text(
                              "Click here",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20),
                            ),

                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.symmetric(vertical: 10),
              width: 380,
              height: 120,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.redAccent.withOpacity(0.6),),
                borderRadius: BorderRadius.circular(0),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.3),
                    spreadRadius: 2,
                    offset: Offset(4,11),
                    blurRadius: 10,
                  )
                ],
              ),
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 5),
                    child: Image.asset("assets/Images/5.jpg"),
                  ),
                  Container(
                    width: 210,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.only(top: 8, left: 10),
                          child: Text(
                            "Jaisalmer",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 10),
                          child: Text(
                            "Explore Jaisalmer 9D/8N",
                            style: TextStyle(
                              fontSize: 12,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 8),
                          padding: EdgeInsets.only(left: 10),
                          child: Text(
                            "RS 19000",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,


                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 0),
                        padding: EdgeInsets.all(4),

                        decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(
                            10,
                          ),
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [

                            Text(
                              "Click here",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20),
                            ),

                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            // Padding(
            //   padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 30),
            //   child: Container(
            //     margin: EdgeInsets.all(20),
            //     padding: EdgeInsets.symmetric(vertical: 10),
            //     decoration: BoxDecoration(
            //       borderRadius: BorderRadius.circular(10),
            //       color: Colors.white,
            //       boxShadow: [
            //         BoxShadow(
            //             color: Colors.grey.withOpacity(0.5),
            //             spreadRadius: 2,
            //             blurRadius: 10,
            //             offset: Offset(0, 3))
            //       ],
            //     ),
            //     child: Column(
            //       children: [
            //         Padding(
            //           padding: EdgeInsets.symmetric(vertical: 10),
            //           child: Row(
            //             children: [
            //               Container(
            //                 margin: EdgeInsets.only(left: 10, ),
            //                 child: Text(
            //                   "Items : ",
            //                   style: TextStyle(fontSize: 20),
            //                 ),
            //               ),
            //               Container(
            //                   padding: EdgeInsets.only(left: 170),
            //                   child: Text(
            //                     "10 : ",
            //                     style: TextStyle(fontSize: 20),
            //                   )),
            //             ],
            //
            //           ),
            //         ),
            //         Container(child: Divider(color: Colors.black,),),
            //
            //         Padding(
            //           padding: EdgeInsets.symmetric(vertical: 10),
            //           child: Row(
            //             children: [
            //               Container(
            //                 margin: EdgeInsets.only(left: 10, ),
            //                 child: Text(
            //                   "Sub total : ",
            //                   style: TextStyle(fontSize: 20),
            //                 ),
            //               ),
            //               Container(
            //                   padding: EdgeInsets.only(left: 120),
            //                   child: Text(
            //                     "Rs0000 : ",
            //                     style: TextStyle(fontSize: 20),
            //                   )),
            //             ],
            //
            //           ),
            //         ),
            //         Container(child: Divider(color: Colors.black,),),
            //
            //         Padding(
            //           padding: EdgeInsets.symmetric(vertical: 10),
            //           child: Row(
            //             children: [
            //               Container(
            //                 margin: EdgeInsets.only(left: 10, ),
            //                 child: Text(
            //                   "Delivery : ",
            //                   style: TextStyle(fontSize: 20),
            //                 ),
            //               ),
            //               Container(
            //                   padding: EdgeInsets.only(left: 120),
            //                   child: Text(
            //                     "Rs 0000 : ",
            //                     style: TextStyle(fontSize: 20),
            //                   )),
            //             ],
            //
            //           ),
            //         ),
            //         Divider(color: Colors.black,),
            //
            //         Padding(
            //           padding: EdgeInsets.symmetric(vertical: 10),
            //           child: Row(
            //             children: [
            //               Container(
            //                 margin: EdgeInsets.only(left: 10, ),
            //                 child: Text(
            //                   "TOTAL : ",
            //                   style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),
            //                 ),
            //               ),
            //               Container(
            //                   padding: EdgeInsets.only(left: 120),
            //                   child: Text(
            //                     "RS 0000 : ",
            //                     style: TextStyle(fontSize: 20,color: Colors.red),
            //                   )),
            //             ],
            //
            //           ),
            //         ),
            //
            //
            //
            //       ],
            //     ),
            //   ),
            // )
          ],
        ),
        bottomNavigationBar: Container(
          padding: EdgeInsets.symmetric(horizontal: 15),
          height: 70,
          child: Row(

            children: [
              // Container(
              //   margin: EdgeInsets.only(left: 10, ),
              //   child: Text(
              //     "TOTAL : ",
              //     style: TextStyle(fontSize: 22,fontWeight: FontWeight.bold),
              //   ),
              // ),
              // SizedBox(width: 10,),
              // Container(
              //
              //     child: Text(
              //       "RS 0000 : ",
              //       style: TextStyle(fontSize: 22,color: Colors.red),
              //     )),
              // SizedBox(width: 35,),
              TextButton(onPressed: (){}, style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.red)),
                child: Text("Book Now ",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20,color: Colors.white),),
              )
            ],
          ),
        ),
      ),
    );
  }
}
