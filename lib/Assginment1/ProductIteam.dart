import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BookingIteamList extends StatelessWidget {
  const BookingIteamList({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: ListView(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.3),
                          spreadRadius: 2,
                          offset: Offset(0, 3),
                          blurRadius: 5,
                        )
                      ],
                    ),
                    child: TextButton(
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(Colors.black)),
                      onPressed: ()
                      {
                        Navigator.pop(
                          context,
                          // MaterialPageRoute(
                          //   builder: (context) => VitalProductList(),
                          // ),
                        );
                      },
                      child: Icon(Icons.arrow_back,color: Colors.white,),

                    ),
                  ),

                  InkWell(
                    onTap: () {},
                    child: Container(
                        margin: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 2,
                                offset: Offset(0, 3),
                                blurRadius: 10,
                              )
                            ]),
                        child: Icon(Icons.notification_add_rounded)),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 5),
              child: Container(
                margin: EdgeInsets.all(10),
                width: 250,
                height: 400,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black.withOpacity(0.6),),
                  borderRadius: BorderRadius.circular(0),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.3),
                      spreadRadius: 2,
                      offset: Offset(4,11),
                      blurRadius: 10,
                    )
                  ],
                ),
                child: Column(
                  children: [
                    // Container(
                    //     margin: EdgeInsets.only(left: 280, top: 10),
                    //     child: Icon(
                    //       Icons.favorite_border_rounded,
                    //       color: Colors.red,
                    //       size: 30,
                    //     )),
                    Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.only(left: 2),
                      margin: EdgeInsets.only(left: 6, bottom: 10),
                      height: 250,
                      width: 250,
                      child: Image.asset("assets/Images/1.jpg"),
                    ),
                    SizedBox(height: 12),
                    Row(
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 20),
                          child: Text(
                            "RS 18000",
                            style: TextStyle(
                              color: Colors.red,
                              fontSize: 20,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 180,
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        // Container(
                        //   margin: EdgeInsets.all(5),
                        //   child: RatingBar.builder(
                        //     initialRating: 5,
                        //     maxRating: 1,
                        //     itemCount: 5,
                        //     itemSize: 18,
                        //     itemPadding: EdgeInsets.symmetric(
                        //         horizontal: 3, vertical: 4),
                        //     itemBuilder: (context, _) => Icon(
                        //       Icons.star,
                        //       color: Colors.red,
                        //     ),
                        //     direction: Axis.horizontal,
                        //     onRatingUpdate: (index) {},
                        //   ),
                        // ),
                        SizedBox(
                          width: 70,
                        ),
                        // Container(
                        //   padding: EdgeInsets.only(left: 130),
                        //   child: Icon(Icons.shopping_cart,
                        //       color: Colors.red, size: 30),
                        // ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    padding: EdgeInsets.only(left: 15),
                    child: Text(
                      "Udaipur ",
                      style:
                          TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                    )),
                Container(
                  padding: EdgeInsets.only(left: 15),
                  child: Text(
                    "Explore Udaipur  ",
                    style: TextStyle(
                      fontSize: 12,
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(5),
                  margin: EdgeInsets.all(10),
                  width: 400,
                  height: 350,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(0),
                    color: Colors.red,
                  ),
                  child: Text(
                    "Lorem Ipsum, sometimes referred to as ‘lipsum’, "
                    "is the placeholder text used in design when creating content. "
                    "It helps designers plan out where the content will sit, without "
                    "needing to wait for the content to be written and approved.It originally "
                    "comes from a Latin text, but to today’s reader, it’s seen as gibberish. "
                    "This is the clever bit, as it helps show what on-page text will look like – "
                    "without distracting from the main focus, the overall design.",
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                )
              ],
            )
          ],
        ),
        bottomNavigationBar: Container(
          padding: EdgeInsets.symmetric(horizontal: 25),
          height: 70,
          child: Row(
            children: [
              // Container(
              //   margin: EdgeInsets.only(
              //     left: 10,
              //   ),
              //   child: Text(
              //     "TOTAL : ",
              //     style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
              //   ),
              // ),
              SizedBox(
                width: 10,
              ),
              // Container(
              //     child: Text(
              //   "RS 0000 : ",
              //   style: TextStyle(fontSize: 22, color: Colors.red),
              // )),
              SizedBox(
                width: 25,
              ),
              TextButton(
                onPressed: () {},
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.red)),
                child: Text(
                  "Book now ",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.white),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
