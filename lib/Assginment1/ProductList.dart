
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class BookingList extends StatefulWidget {
  @override
  State<BookingList> createState() => _BookingListState();
}

class _BookingListState extends State<BookingList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          //appBar
          Padding(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  onTap: () {},
                  child: Container(
                      margin: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.circular(20),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.red.withOpacity(0.3),
                            spreadRadius: 2,
                            offset: Offset(0, 3),
                            blurRadius: 10,
                          )
                        ],
                      ),
                      child: Container(padding:EdgeInsets.all(5),child: Icon(CupertinoIcons.bars,color: Colors.white,))),
                ),
                // InkWell(
                //   onTap: () {},
                //   child: Container(
                //       margin: EdgeInsets.all(10),
                //       decoration: BoxDecoration(
                //           color: Colors.white,
                //           borderRadius: BorderRadius.circular(0),
                //           boxShadow: [
                //             BoxShadow(
                //               color: Colors.grey.withOpacity(0.5),
                //               spreadRadius: 2,
                //               offset: Offset(0, 3),
                //               blurRadius: 10,
                //             )
                //           ]),
                //       child: Icon(Icons.notification_add_rounded)),
                // ),
              ],
            ),
          ),
          //SerchField
          Container(
            padding: const EdgeInsets.all(4.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(0),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 2,
                    blurRadius: 10,
                    offset: Offset(0, 3))
              ],
            ),
            margin: EdgeInsets.all(15),
            child: TextField(
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.search, color: Colors.red),
                hintText: 'Find your destination ',
                suffixIcon: Icon(Icons.filter_list),
              ),
            ),
          ),
          //Catagaories
          Container(
            padding: EdgeInsets.only(top: 10, left: 10),
            child: Text(
              "Categaories",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
          ),
          //Scroll Clock
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                Column(
                  children: [
                    Container(
                      padding: EdgeInsets.all(8),
                      margin: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black.withOpacity(0.6),),
                        borderRadius: BorderRadius.circular(0),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.3),
                            spreadRadius: 2,
                            offset: Offset(4,7),
                            blurRadius: 10,
                          )
                        ],
                      ),
                      child: Image.asset(
                        "assets/Images/6.jpg",
                        width: 50,
                        height: 50,
                      ),
                    ),
                    Text(
                      "Hotel booking",
                      style: TextStyle(fontSize: 12),
                    )
                  ],
                ),
                Column(
                  children: [
                    Container(
                      padding: EdgeInsets.all(8),
                      margin: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black.withOpacity(0.6),),
                        borderRadius: BorderRadius.circular(0),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.3),
                            spreadRadius: 2,
                            offset: Offset(4,7),
                            blurRadius: 10,
                          )
                        ],
                      ),
                      child: Image.asset(
                        "assets/Images/7.jpg",
                        width: 50,
                        height: 50,
                      ),
                    ),
                    Text(
                      "Flight booking",
                      style: TextStyle(fontSize: 12,),
                    )
                  ],
                ),
                Column(
                  children: [
                    Container(
                      padding: EdgeInsets.all(8),
                      margin: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black.withOpacity(0.6),),
                        borderRadius: BorderRadius.circular(0),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.3),
                            spreadRadius: 2,
                            offset: Offset(4,7),
                            blurRadius: 10,
                          )
                        ],
                      ),
                      child: Image.asset(
                        "assets/Images/8.jpg",
                        width: 50,
                        height: 50,
                      ),
                    ),
                    Text(
                      "Train bookin",
                      style: TextStyle(fontSize: 12,),
                    )
                  ],
                ),
                // Column(
                //   children: [
                //     Container(
                //       padding: EdgeInsets.all(8),
                //       margin: EdgeInsets.all(10),
                //       decoration: BoxDecoration(
                //         border: Border.all(color: Colors.redAccent.withOpacity(0.6),),
                //         borderRadius: BorderRadius.circular(15),
                //         color: Colors.white,
                //         boxShadow: [
                //           BoxShadow(
                //             color: Colors.deepOrangeAccent.withOpacity(0.3),
                //             spreadRadius: 2,
                //             offset: Offset(4,7),
                //             blurRadius: 10,
                //           )
                //         ],
                //       ),
                //       child: Image.asset(
                //         "assets/image/Clock/2walll.jpg",
                //         width: 50,
                //         height: 50,
                //       ),
                //     ),
                //     Text(
                //       "Woond  clock",
                //       style: TextStyle(fontSize: 12,),
                //     )
                //   ],
                // ),
                // Column(
                //   children: [
                //     Container(
                //       padding: EdgeInsets.all(8),
                //       margin: EdgeInsets.all(10),
                //       decoration: BoxDecoration(
                //         border: Border.all(color: Colors.redAccent.withOpacity(0.6),),
                //         borderRadius: BorderRadius.circular(15),
                //         color: Colors.white,
                //         boxShadow: [
                //           BoxShadow(
                //             color: Colors.deepOrangeAccent.withOpacity(0.3),
                //             spreadRadius: 2,
                //             offset: Offset(4,7),
                //             blurRadius: 10,
                //           )
                //         ],
                //       ),
                //       child: Image.asset(
                //         "assets/image/Clock/5walll.jpg",
                //         width: 50,
                //         height: 50,
                //       ),
                //     ),
                //     Text(
                //       "fancy With Digital",
                //       style: TextStyle(fontSize: 12,),
                //     )
                //   ],
                // ),
                // Column(
                //   children: [
                //     Container(
                //       padding: EdgeInsets.all(8),
                //       margin: EdgeInsets.all(10),
                //       decoration: BoxDecoration(
                //         border: Border.all(color: Colors.redAccent.withOpacity(0.6),),
                //         borderRadius: BorderRadius.circular(15),
                //         color: Colors.white,
                //         boxShadow: [
                //           BoxShadow(
                //             color: Colors.deepOrangeAccent.withOpacity(0.3),
                //             spreadRadius: 2,
                //             offset: Offset(4,7),
                //             blurRadius: 10,
                //           )
                //         ],
                //       ),
                //       child: Image.asset(
                //         "assets/image/Clock/6wall.jpg",
                //         width: 50,
                //         height: 50,
                //       ),
                //     ),
                //     Text(
                //       "Dgital Clock",
                //       style: TextStyle(fontSize: 12,),
                //     )
                //   ],
                // ),
              ],
            ),
          ),
          //Populer Clock
          Container(
            padding: EdgeInsets.only(top: 20, left: 10),
            child: Text(
              "Popular Destination",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
          ),
          //Populer Clock Display
          Row(
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 15, horizontal: 5),
                child: Container(

                  margin: EdgeInsets.all(10),
                  width: 170,
                  height: 250,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black.withOpacity(0.6),),
                    borderRadius: BorderRadius.circular(0),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.3),
                        spreadRadius: 2,
                        offset: Offset(4,11),
                        blurRadius: 10,
                      )
                    ],
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.only(left: 10),
                        margin: EdgeInsets.only(left: 8, top: 10),
                        height: 150,
                        width: 150,
                        child: Image.asset("assets/Images/9.jpg"),
                      ),
                      Container(
                          padding: EdgeInsets.only(left: 15),
                          child: Text(
                            "Kashmir",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          )),
                      Container(
                        padding: EdgeInsets.only(left: 15),
                        child: Text(
                          "Explore Kashmir with us  ",
                          style: TextStyle(
                            fontSize: 12,
                          ),
                        ),
                      ),
                      SizedBox(height: 12),
                      Row(
                        children: [
                          Container(
                            padding: EdgeInsets.only(left: 20),
                            child: Text(
                              "RS 56000",
                              style: TextStyle(
                                color: Colors.red,
                                fontSize: 15,
                              ),
                            ),
                          ),
                          Container(
                              // margin: EdgeInsets.only(left: 50),
                              // child: Icon(
                              //   Icons.favorite_border_rounded,
                              //   color: Colors.red,
                              // )
                            )
                        ],
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 15, horizontal: 5),
                child: Container(
                  margin: EdgeInsets.all(10),
                  width: 170,
                  height: 250,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black.withOpacity(0.6),),
                    borderRadius: BorderRadius.circular(0),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.3),
                        spreadRadius: 2,
                        offset: Offset(4,11),
                        blurRadius: 10,
                      )
                    ],
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.only(left: 10),
                        margin: EdgeInsets.only(left: 8, top: 10),
                        height: 150,
                        width: 150,
                        child: Image.asset("assets/Images/10.jpg"),
                      ),
                      Container(
                          padding: EdgeInsets.only(left: 15),
                          child: Text(
                            "Manali ",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          )),
                      Container(
                        padding: EdgeInsets.only(left: 15),
                        child: Text(
                          "Explore Manali with us  ",
                          style: TextStyle(
                            fontSize: 12,
                          ),
                        ),
                      ),
                      SizedBox(height: 12),
                      Row(
                        children: [
                          Container(
                            padding: EdgeInsets.only(left: 20),
                            child: Text(
                              "RS 45000",
                              style: TextStyle(
                                color: Colors.red,
                                fontSize: 15,
                              ),
                            ),
                          ),
                          Container(
                              margin: EdgeInsets.only(left: 50),
                              // child: Icon(
                              //   //Icons.favorite_border_rounded,
                              //   //color: Colors.red,
                              // )
                            )
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),

          //Newst Clock
          Container(
            padding: EdgeInsets.only(top: 10, left: 10),
            child: Text(
              "Upcoming Tourse",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
          ),
          ////Newst Clock Display
          Container(
            padding: EdgeInsets.all(8.0),
            margin: EdgeInsets.only(top: 20,left: 10,right: 10,),
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(
                    vertical: 0,
                  ),
                ),
                Column(
                  children: [
                    Container(
                      height: 160,
                      width: 400,
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black.withOpacity(0.6),),
                        borderRadius: BorderRadius.circular(0),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.3),
                            spreadRadius: 2,
                            offset: Offset(4,11),
                            blurRadius: 10,
                          )
                        ],
                      ),
                      child: Row(
                        children: [
                          InkWell(
                            onTap: () {},
                            child: Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.only(left: 10),
                              margin: EdgeInsets.only(top: 5, bottom: 20),
                              height: 160,
                              width: 160,
                              child: InkWell(
                                  // onTap: () {
                                  //   Navigator.push(
                                  //     context,
                                  //     MaterialPageRoute(
                                  //       builder: (context) => _VitalProductIteam(),
                                  //     ),
                                  //   );
                                  // },
                                  child: Image.asset(
                                      "assets/Images/11.jpg")),
                            ),
                          ),
                          Container(
                            width: 200,
                            child: Container(
                              margin: EdgeInsets.only(top: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(left: 15),
                                    child: Text(
                                      "Mumbai ",
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(left: 15),
                                    child: Text(
                                      "Visit mumbai  ",
                                      style: TextStyle(
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(left: 15),
                                    child: Text(
                                      "With us ",
                                      style: TextStyle(
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                  // Container(
                                  //   margin: EdgeInsets.only(top: 5),
                                  //   padding: EdgeInsets.only(left: 10),
                                  //   child: RatingBar.builder(
                                  //     initialRating: 5,
                                  //     maxRating: 1,
                                  //     itemCount: 5,
                                  //     itemSize: 18,
                                  //     itemPadding: EdgeInsets.symmetric(
                                  //         horizontal: 3, vertical: 4),
                                  //     itemBuilder: (context, _) => Icon(
                                  //       Icons.star,
                                  //       color: Colors.red,
                                  //     ),
                                  //     direction: Axis.horizontal,
                                  //     onRatingUpdate: (index) {},
                                  //   ),
                                  // ),
                                  Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: Text(
                                      "RS 15000",
                                      style: TextStyle(
                                        color: Colors.red,
                                        fontSize: 15,
                                      ),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      // Container(
                                      //   padding: EdgeInsets.only(left: 10),
                                      //   child: Icon(
                                      //     Icons.favorite_border_rounded,
                                      //     color: Colors.red,
                                      //   ),
                                      // ),
                                      // Container(
                                      //   padding: EdgeInsets.only(left: 130),
                                      //   child: Icon(
                                      //     Icons.shopping_cart,
                                      //     color: Colors.red,
                                      //   ),
                                      // ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      height: 160,
                      width: 400,
                      margin: EdgeInsets.only(top: 15, ),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black.withOpacity(0.6),),
                        borderRadius: BorderRadius.circular(0),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.3),
                            spreadRadius: 2,
                            offset: Offset(4,11),
                            blurRadius: 10,
                          )
                        ],
                      ),
                      child: Row(
                        children: [
                          InkWell(
                            onTap: () {},
                            child: Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.only(left: 10),
                              margin: EdgeInsets.only(top: 10, ),
                              height: 160,
                              width: 160,
                              child:
                                  Image.asset("assets/Images/12.jpg"),
                            ),
                          ),
                          Container(
                            width: 200,
                            child: Container(
                              margin: EdgeInsets.only(top: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(left: 15),
                                    child: Text(
                                      "Madhyapradesh ",
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(left: 15),
                                    child: Text(
                                      "Visit madhyapradesh  ",
                                      style: TextStyle(
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(left: 15),
                                    child: Text(
                                      "with us ",
                                      style: TextStyle(
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                  // Container(
                                  //   margin: EdgeInsets.only(top: 5),
                                  //   padding: EdgeInsets.only(left: 10),
                                  //   child: RatingBar.builder(
                                  //     initialRating: 5,
                                  //     maxRating: 1,
                                  //     itemCount: 5,
                                  //     itemSize: 18,
                                  //     itemPadding: EdgeInsets.symmetric(
                                  //         horizontal: 3, vertical: 4),
                                  //     itemBuilder: (context, _) => Icon(
                                  //       Icons.star,
                                  //       color: Colors.red,
                                  //     ),
                                  //     direction: Axis.horizontal,
                                  //     onRatingUpdate: (index) {},
                                  //   ),
                                  // ),
                                  Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: Text(
                                      "RS 18000",
                                      style: TextStyle(
                                        color: Colors.red,
                                        fontSize: 15,
                                      ),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      // Container(
                                      //   padding: EdgeInsets.only(left: 10),
                                      //   child: Icon(
                                      //     Icons.favorite_border_rounded,
                                      //     color: Colors.red,
                                      //   ),
                                      // ),
                                      // Container(
                                      //   padding: EdgeInsets.only(left: 130),
                                      //   child: Icon(
                                      //     Icons.shopping_cart,
                                      //     color: Colors.red,
                                      //   ),
                                      // ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      height: 160,
                      width: 400,
                      margin: EdgeInsets.only(top: 15, ),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black.withOpacity(0.6),),
                        borderRadius: BorderRadius.circular(0),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.3),
                            spreadRadius: 2,
                            offset: Offset(4,11),
                            blurRadius: 10,
                          )
                        ],
                      ),
                      child: Row(
                        children: [
                          InkWell(
                            onTap: () {},
                            child: Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.only(left: 10),
                              margin: EdgeInsets.only(top: 5, bottom: 20),
                              height: 160,
                              width: 160,
                              child:
                                  Image.asset("assets/Images/13.jpg"),
                            ),
                          ),
                          Container(
                            width: 200,
                            child: Container(
                              margin: EdgeInsets.only(top: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(left: 15),
                                    child: Text(
                                      "Gujrat",
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(left: 15),
                                    child: Text(
                                      "Visit gujrat  ",
                                      style: TextStyle(
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(left: 15),
                                    child: Text(
                                      "With us ",
                                      style: TextStyle(
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                  // Container(
                                  //   margin: EdgeInsets.only(top: 5),
                                  //   padding: EdgeInsets.only(left: 10),
                                  //   child: RatingBar.builder(
                                  //     initialRating: 5,
                                  //     maxRating: 1,
                                  //     itemCount: 5,
                                  //     itemSize: 18,
                                  //     itemPadding: EdgeInsets.symmetric(
                                  //         horizontal: 3, vertical: 4),
                                  //     itemBuilder: (context, _) => Icon(
                                  //       Icons.star,
                                  //       color: Colors.red,
                                  //     ),
                                  //     direction: Axis.horizontal,
                                  //     onRatingUpdate: (index) {},
                                  //   ),
                                  // ),
                                  Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: Text(
                                      "RS 22000",
                                      style: TextStyle(
                                        color: Colors.red,
                                        fontSize: 15,
                                      ),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      // Container(
                                      //   padding: EdgeInsets.only(left: 10),
                                      //   child: Icon(
                                      //     Icons.favorite_border_rounded,
                                      //     color: Colors.red,
                                      //   ),
                                      // ),
                                      // Container(
                                      //   padding: EdgeInsets.only(left: 130),
                                      //   child: Icon(
                                      //     Icons.shopping_cart,
                                      //     color: Colors.red,
                                      //   ),
                                      // ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Container(
                      height: 160,
                      width: 400,
                      margin: EdgeInsets.only(top: 15, ),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black.withOpacity(0.6),),
                        borderRadius: BorderRadius.circular(0),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.3),
                            spreadRadius: 2,
                            offset: Offset(4,11),
                            blurRadius: 10,
                          )
                        ],
                      ),
                      child: Row(
                        children: [
                          InkWell(
                            onTap: () {},
                            child: Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.only(left: 10),
                              margin: EdgeInsets.only(top: 5, bottom: 20),
                              height: 160,
                              width: 160,
                              child:
                                  Image.asset("assets/Images/14.jpg"),
                            ),
                          ),
                          Container(
                            width: 200,
                            child: Container(
                              margin: EdgeInsets.only(top: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(left: 15),
                                    child: Text(
                                      "Punjab ",
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(left: 12),
                                    child: Text(
                                      " Visit punjab  ",
                                      style: TextStyle(
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(left: 15),
                                    child: Text(
                                      "With us ",
                                      style: TextStyle(
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                  // Container(
                                  //   margin: EdgeInsets.only(top: 5),
                                  //   padding: EdgeInsets.only(left: 10),
                                  //   child: RatingBar.builder(
                                  //     initialRating: 5,
                                  //     maxRating: 1,
                                  //     itemCount: 5,
                                  //     itemSize: 18,
                                  //     itemPadding: EdgeInsets.symmetric(
                                  //         horizontal: 3, vertical: 4),
                                  //     itemBuilder: (context, _) => Icon(
                                  //       Icons.star,
                                  //       color: Colors.red,
                                  //     ),
                                  //     direction: Axis.horizontal,
                                  //     onRatingUpdate: (index) {},
                                  //   ),
                                  // ),
                                  Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: Text(
                                      "RS 18000",
                                      style: TextStyle(
                                        color: Colors.red,
                                        fontSize: 15,
                                      ),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      // Container(
                                      //   padding: EdgeInsets.only(left: 10),
                                      //   child: Icon(
                                      //     Icons.favorite_border_rounded,
                                      //     color: Colors.red,
                                      //   ),
                                      // ),
                                      // Container(
                                      //   padding: EdgeInsets.only(left: 130),
                                      //   child: Icon(
                                      //     Icons.shopping_cart,
                                      //     color: Colors.red,
                                      //   ),
                                      // ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    //   bottomNavigationBar: CurvedNavigationBar(
    //     backgroundColor: Colors.blueAccent,
    //     items: [
    //       CurvedNavigationBarItem(
    //         child: InkWell(
    //             onTap: () {
    //               Navigator.push(
    //                 context,
    //                 MaterialPageRoute(
    //                   builder: (context) => VitalSignIn(),
    //                 ),
    //               );
    //             },
    //             child: Icon(
    //               Icons.home_outlined,
    //               color: Colors.red,
    //             )),
    //         label: 'Home',
    //       ),
    //       CurvedNavigationBarItem(
    //         child: InkWell(
    //             onTap: () {
    //               Navigator.push(
    //                 context,
    //                 MaterialPageRoute(
    //                   builder: (context) => Vitalcart(),
    //                 ),
    //               );
    //             },
    //             child: Icon(
    //               Icons.shopping_cart_checkout,
    //               color: Colors.red,
    //             )),
    //         label: 'Cart',
    //       ),
    //       CurvedNavigationBarItem(
    //         child: Icon(
    //           Icons.chat_bubble_outline,
    //           color: Colors.red,
    //         ),
    //         label: 'Customer Care',
    //       ),
    //       CurvedNavigationBarItem(
    //         child: Icon(
    //           Icons.newspaper,
    //           color: Colors.red,
    //         ),
    //         label: 'Feed back',
    //       ),
    //       CurvedNavigationBarItem(
    //         child: Icon(
    //           Icons.perm_identity,
    //           color: Colors.red,
    //         ),
    //         label: 'Personal',
    //       ),
    //     ],
    //     onTap: (index) {
    //       // Handle button tap
    //     },
    //   ),
     );
  }
}
